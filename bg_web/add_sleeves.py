from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import InputRequired

from .db import get_db_connection


def update_sleeve_db(form):
    conn = get_db_connection()
    conn.set_trace_callback(print)
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO Sleeves (Brand, Type, Size, Qty) VALUES (?,?,?,?) ON CONFLICT(Brand, Type, Size) DO UPDATE SET "
        "Qty = Qty + ?",
        (
            form.brand.data,
            form.stype.data,
            form.size.data,
            form.qty.data,
            form.qty.data,
        ),
    )
    conn.commit()


class AddSleeveForm(FlaskForm):
    brand = StringField("Sleeve Brand", validators=[InputRequired()])
    stype = StringField("Type", validators=[InputRequired()])
    size = StringField("Size", validators=[InputRequired()])
    qty = IntegerField("Qty", validators=[InputRequired()])
