#!/usr/bin/env python3

import sys
import sqlite3
import csv

CSV_FILE = sys.argv[1]
DB_FILE = sys.argv[2]

DB = sqlite3.connect(DB_FILE, detect_types=sqlite3.PARSE_DECLTYPES)
DB.row_factory = sqlite3.Row

with open(CSV_FILE, "r") as csvfile:
    csr = csv.reader(csvfile)
    cur = DB.cursor()
    next(csr)
    for row in csr:
        cur.execute("INSERT OR IGNORE INTO BGG_Games VALUES (?, ?);", (row[0], row[1]))
    DB.commit()
