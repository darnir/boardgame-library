#!/usr/bin/env python3

import io
import os
import tarfile

from flask import current_app, send_file, render_template

app = current_app


@app.route("/")
def index():
    """Display the homepage for the web app."""
    return render_template("index.html")


def tarfilter(obj: tarfile.TarInfo):
    new_obj = obj.replace(
        name=os.path.relpath("/" + obj.name, start=os.path.join(app.root_path, "..")),
        deep=False,
    )
    return (
        None
        if new_obj.name == os.path.join(os.path.basename(app.root_path), "__pycache__")
        else obj
    )


@app.route("/agpl")
def agpl():
    buf = io.BytesIO()
    with tarfile.open(fileobj=buf, mode="w|gz") as tar:
        tar.add(app.root_path, filter=tarfilter)
    buf.seek(0)
    return send_file(
        buf,
        mimetype="application/gzip",
        download_name="sources.tar.gz",
        as_attachment=True,
    )
