#!/usr/bin/env python3

from flask import (
    Blueprint,
    render_template,
)

from bg_web.add_sleeves import AddSleeveForm, update_sleeve_db
from bg_web.db import get_db_connection

bp = Blueprint("sleeves", __name__, url_prefix="/sleeves")


@bp.route("/to-buy")
def tobuy():
    """List the sleeves that need purchasing based on inventory and requirements."""
    db = get_db_connection()
    tobuy = db.execute("SELECT * FROM Purchase").fetchall()
    return render_template("sleeves.html", sleeves=tobuy)


@bp.route("/stock", methods=("GET", "POST"))
def stock():
    """List the sleeves available in stock."""
    conn = get_db_connection()
    instock = conn.execute("SELECT * FROM SleeveInformation").fetchall()
    return render_template("stock.html", stock=instock)


def getColumn(col):
    conn = get_db_connection()
    cur = conn.cursor()
    res = [c[0] for c in cur.execute("SELECT DISTINCT %s FROM Sleeves" % col)]
    return res


@bp.route("/add", methods=("GET", "POST"))
def add():
    """Add a new game and the cards sizes."""
    form = AddSleeveForm()
    if form.is_submitted():
        print("is submitted")
        if form.validate():
            print("validated")
        else:
            print("validation failed")
    else:
        print("Not submitted")
    if form.validate_on_submit():
        print("updating")
        update_sleeve_db(form)
    else:
        print("failed")
    return render_template(
        "add_sleeves.html",
        form=form,
        brands=getColumn("Brand"),
        stypes=getColumn("Type"),
        sizes=getColumn("Size"),
    )
