#!/usr/bin/env python3

from flask import Blueprint, flash, render_template, request, url_for, redirect
from boardgamegeek import BGGClient

from typing import Dict, List
from sqlite3 import Row as sqlite3Row

from bg_web.db import get_db_connection, close_db
from bg_web.add_card import SingleCard

bp = Blueprint("games", __name__, url_prefix="/games")


@bp.route("/sync")
def sync_games():
    db = get_db_connection()
    bgg = BGGClient()
    my_games = [g.id for g in bgg.collection("frakazam", own=True)]
    cur = db.cursor()
    db_games = [g[0] for g in cur.execute("SELECT Id FROM OwnedGames").fetchall()]
    to_remove = set(db_games).difference(my_games)
    to_add = set(my_games).difference(db_games)

    if len(to_add) != 0:
        add_query = f"SELECT id, name FROM BGG_Games WHERE id in ( {','.join(['?'] * len(to_add))} )"
        new_games = dict(cur.execute(add_query, list(to_add)).fetchall())
    else:
        new_games = dict()

    if len(to_remove) != 0:
        rem_query = f"SELECT id, name FROM BGG_Games WHERE id in ( {','.join(['?'] * len(to_remove))} )"
        prev_games = dict(cur.execute(rem_query, sorted(to_remove)).fetchall())
    else:
        prev_games = dict()

    cur.close()
    db.commit()
    final = {"New Games": new_games, "Games to remove": prev_games}
    return final


@bp.route("/list")
def list():
    db = get_db_connection()
    games = db.execute("SELECT * FROM OwnedGamesView;").fetchall()
    cards = db.execute(
        'SELECT GameId, Cards.Size as cardSize, Cards.SleeveId, Sleeves.Brand || " " || Sleeves.Type AS Sleeve, Sleeves.Size as SleeveSize, Cards.Qty, Cards.Sleeved FROM cards INNER JOIN sleeves USING (SleeveId)'
    ).fetchall()
    sleeves = db.execute('SELECT SleeveId, Brand, Size FROM Sleeves').fetchall()
    card_dict: Dict[int, List[sqlite3Row]] = dict()
    for card in cards:
        gid = card["GameId"]
        if gid in card_dict.keys():
            card_dict[gid].append(card)
        else:
            card_dict[gid] = [card]
    return render_template("games.html", games=games, cards=card_dict, sleeves=sleeves)


def process_add_game(db, all_games: Dict[int, str]):
    try:
        _id, name = request.form["bg_name"].split(": ", 1)
        id = int(_id)
    except ValueError:
        return redirect(url_for("games.add_new_game"))

    if id not in all_games:
        flash(
            f'Error: Game "{name}" does not exist in DB. Please check the spelling and retry'
        )
        return redirect(url_for("games.add_new_game"))

    owned_games = dict(db.execute("SELECT * FROM OwnedGamesView;").fetchall())
    if id in owned_games:
        flash(f'Error: Game "{name}" is already owned.')
        return redirect(url_for("games.add_new_game"))

    db.execute("INSERT INTO OwnedGames VALUES(?)", (id,))
    db.commit()

    return redirect(url_for("games.add_new_game"))


@bp.route("/add", methods=("GET", "POST"))
def add_new_game():
    db = get_db_connection()
    all_games = db.execute("SELECT Id, Name FROM BGG_Games;").fetchall()
    if request.method == "POST":
        return process_add_game(db, dict(all_games))
    else:
        close_db()
    return render_template("add_game.html", games=all_games)


def process_add_cards(db, gamelist: Dict[int, str]):
    name = request.form["select-game"]
    if name not in gamelist.values():
        flash(
            f'Error: Game "{name}" does not exist in DB. Please check the spelling or add it to the DB first.'
        )
        return redirect(url_for("games.add_cards"), code=400)

    (id,) = db.execute(
        "SELECT Id from OwnedGamesView AS ogl WHERE ogl.Name == ?", (name,)
    ).fetchall()[0]

    for sleeve, card_size, qty in zip(
        request.form.getlist("sleeveId"),
        request.form.getlist("card-size"),
        request.form.getlist("cQty"),
    ):
        print(f"{id} {sleeve} {card_size} {qty} 0")
        db.execute(
            "INSERT INTO Cards VALUES(?, ?, ?, ?, ?)", (id, sleeve, card_size, qty, 0)
        )

    db.commit()
    return redirect(url_for("games.add_cards"), code=303)


@bp.route("/add-cards", methods=("GET", "POST"))
def add_cards():
    db = get_db_connection()
    card = SingleCard()
    gamelist = db.execute(
        "SELECT bg.id, bg.Name from BGG_Games as bg, OwnedGames as og WHERE og.id == bg.id"
    ).fetchall()

    if request.method == "POST":
        return process_add_cards(db, dict(gamelist))

    card_sizes = db.execute("SELECT size FROM Cards").fetchall()

    sleeve_list = {}
    for sid, brand, stype, size in db.execute(
        "SELECT SleeveId, Brand, Type, Size FROM Sleeves"
    ):
        sleeve_list[sid] = f"{brand} {stype} {size}"

    card.sleeve.choices = [(k, v) for k, v in sleeve_list.items()]

    return render_template(
        "add_cards.html", games=gamelist, card_sizes=card_sizes, sleeves=sleeve_list, card=card
    )
