BEGIN TRANSACTION;
DROP TABLE IF EXISTS "BGG_Games";
CREATE TABLE IF NOT EXISTS "BGG_Games" (
	"Id"	INTEGER NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL,
	PRIMARY KEY("Id")
);
DROP TABLE IF EXISTS "OwnedGames";
CREATE TABLE IF NOT EXISTS "OwnedGames" (
	"Id"	INTEGER NOT NULL UNIQUE,
	FOREIGN KEY("Id") REFERENCES "BGG_Games"("Id"),
	PRIMARY KEY("Id")
);
DROP TABLE IF EXISTS "Sleeves";
CREATE TABLE IF NOT EXISTS "Sleeves" (
	"SleeveId"	INTEGER NOT NULL UNIQUE,
	"Brand"	TEXT NOT NULL DEFAULT 'Swan PanAsia',
	"Size"	TEXT NOT NULL,
	"Type"	TEXT NOT NULL DEFAULT 'Premium',
	"Qty"	INTEGER NOT NULL,
	"Pcs Per Pack"	INTEGER DEFAULT 100,
	"Cost Per Pack"	REAL DEFAULT 3.5,
	"Checked"	INTEGER DEFAULT 0,
	UNIQUE("Brand","Size","Type"),
	PRIMARY KEY("SleeveId" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "Cards";
CREATE TABLE IF NOT EXISTS "Cards" (
	"GameId"	INTEGER NOT NULL,
	"SleeveId"	INTEGER,
	"Size"	TEXT NOT NULL,
	"Qty"	INTEGER NOT NULL DEFAULT 0,
	"Sleeved"	INTEGER NOT NULL DEFAULT 0 CHECK("Sleeved" <= "Qty"),
	FOREIGN KEY("SleeveId") REFERENCES "Sleeves"("SleeveId"),
	FOREIGN KEY("GameId") REFERENCES "OwnedGames"("Id"),
	PRIMARY KEY("GameId","SleeveId","Size")
);
DROP VIEW IF EXISTS "OwnedGamesView";
CREATE VIEW "OwnedGamesView" AS SELECT bg.id, bg.name FROM BGG_Games as bg, OwnedGames where bg.id == OwnedGames.id ORDER BY bg.name;
DROP VIEW IF EXISTS "SleeveInformation";
CREATE VIEW "SleeveInformation" AS
SELECT 
  Sleeves.SleeveId, 
  Brand, 
  Sleeves.Size, 
  Type, 
  used.u as Used,
  Sleeves.Qty - IFNULL(used.u, 0) as Stock, 
  needed.n - used.u as Needed,
  CAST(CEIL(CAST (needed.n - used.u as real) / Sleeves."Pcs per pack") as int) as Packs,
  Sleeves."Cost per pack" * CEIL(CAST (needed.n - used.u as real) / Sleeves."Pcs per pack") as "Total Cost"
FROM Sleeves 
LEFT JOIN 
	(
		SELECT 
			SleeveId, SUM(Sleeved) AS u 
		FROM Cards 
		Group By (SleeveId)
	) AS used
	ON Sleeves.SleeveId = used.SleeveId
LEFT JOIN
	(
		SELECT SleeveId, SUM(Qty) as n
		FROM Cards
		GROUP BY (SleeveId)
	) as needed
	ON Sleeves.SleeveId = needed.SleeveId;
COMMIT;
