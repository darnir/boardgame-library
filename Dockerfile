FROM python:slim

LABEL Maintainer "Darshit Shah <git@darnir.net>"

COPY bg_web requirements.txt /bg_web/

WORKDIR /bg_web

RUN pip install -r requirements.txt

EXPOSE 5000

CMD flask run --host 0.0.0.0
